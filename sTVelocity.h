#ifndef TVELOCITY_H_
#define TVELOCITY_H_

class TVelocity
{	
public:
	int dX, dY;

	TVelocity() {dX = 0; dY = 0; }
	TVelocity(const int dX, const int dY) { this->dX = dX; this->dY = dY; }
	TVelocity(const TVelocity& v) { this->dX = v.dX; this->dY = v.dY; }

	TVelocity operator + (const TVelocity& v)
	{
		return TVelocity(dX + v.dX, dY + v.dY);
	}

	TVelocity operator - (const TVelocity& v)
	{
		return TVelocity(dX - v.dX, dY - v.dY);
	}

	void operator += (const TVelocity& v)
	{
		this->dX += v.dX;
		this->dY += v.dY;
	}

	void operator -= (const TVelocity& v)
	{
		this->dX -= v.dX;
		this->dY -= v.dY;
	}
};

#endif /* TVELOCITY_H_ */