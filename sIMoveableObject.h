#ifndef IMOVEABLEOBJECT_H_
#define IMOVEABLEOBJECT_H_

#include "common.h"
#include "sTVelocity.h"

class IMoveableObject : public IGraphicsObject
{
protected:
	TVelocity mVelocity;
public:
	IMoveableObject() : mVelocity(0,0) {}
	IMoveableObject(const TPoint& origin, const TVelocity& velocity) : mVelocity(velocity) 
	{ 
		this->mOrigin = IDrawable(origin); 
	}

	void SetVelocity(const TVelocity& velocity) { this->mVelocity = velocity; }
	TVelocity& GetVelocity() { return mVelocity; }

	virtual void draw() = 0;
	virtual void update() { mOrigin += mVelocity; }
};

#endif /* IMOVEABLEOBJECT_H_ */