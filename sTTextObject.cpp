#include "sTTextObject.h"

string TTextObject::mFontLocation = DEFAULT_FONT_LOCATION;

void TTextObject::update_font()
{
	mFont = TTF_OpenFont(mFontLocation.c_str(), mFontSize);

	try
	{
		validate_font();
	}
	catch (invalid_argument ex)
	{
		cerr << ex.what();
		error_report("TTF_OpenFont");
	}
}

void TTextObject::create_text_texture()
{
	if (mTexture != nullptr)
	{
		SDL_DestroyTexture(mTexture);
		mTexture = nullptr;
	}

	SDL_Surface* surface = TTF_RenderText_Solid(mFont, mText.c_str(), mColor);
	mTexture = surface_to_texture(surface);

	SDL_QueryTexture(mTexture, nullptr, nullptr, &mRect.w, &mRect.h);
	mRect.x = mOrigin.x;
	mRect.y = mOrigin.y;
}

SDL_Texture* TTextObject::surface_to_texture(SDL_Surface* surface)
{
	_RMutex.lock();

	SDL_Texture* texture = SDL_CreateTextureFromSurface(_R, surface);
	
	_RMutex.unlock();

	SDL_FreeSurface(surface);

	return texture;
}

void TTextObject::validate_font()
{
	if (mFont == nullptr) //Unable to open the font
	{
		mFontLocation = DEFAULT_FONT_LOCATION;
		mFont = TTF_OpenFont(mFontLocation.c_str(), mFontSize);

		if (mFont == nullptr)
		{
			throw new invalid_argument("Unable to locate the font file: \"" + string(DEFAULT_FONT_LOCATION) + "\"");
		}
	}
}

TTextObject::TTextObject(const string& Text=DEFAULT_TEXT, int FontSize = DEFAULT_FONT_SIZE, const TPoint& Origin = TPoint(0,0), const SDL_Color Color = DEFAULT_COLOR)
{
	this->mFontSize = FontSize;
	this->mText = Text;
	this->mOrigin = Origin;
	mTexture = nullptr;
	this->mColor = Color;	

	update_font();
	create_text_texture();
}

TTextObject::~TTextObject()
{
	if (mTexture != nullptr)
	{
		SDL_DestroyTexture(mTexture);
		mTexture = nullptr;
	}
}

void TTextObject::SetFont(const string& FontLocation)
{
	this->mFontLocation = FontLocation;
	update_font();
	create_text_texture();
}

void TTextObject::SetFontSize(const int FontSize)
{
	this->mFontSize = FontSize;
	update_font();
	create_text_texture();
}

void TTextObject::SetText(const string& Text)
{
	this->mText = Text;
	create_text_texture();
}

void TTextObject::SetColor(const SDL_Color& Color)
{
	this->mColor = Color;
	create_text_texture();
}

void TTextObject::SetOrigin(const TPoint& origin)
{
	this->mOrigin = origin;
	create_text_texture();
}

void TTextObject::draw()
{
	SDL_RenderCopy(_R, mTexture, nullptr, &mRect);
	SDL_RenderPresent(_R);
}