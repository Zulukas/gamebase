#ifndef COMMON_H_
#define COMMON_H_

// SDL2 Library Includes
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL2_framerate.h>
#include <SDL2/SDL2_gfxPrimitives.h>

#include "sIGraphicsObject.h"

// Standard Library Includes
#include <exception>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

// Standard Library Namespace Definitions
using std::cerr;
using std::cout;
using std::endl;
using std::invalid_argument;
using std::mutex;
using std::string;
using std::thread;
using std::vector;

typedef SDL_Window* WINDOW;
typedef SDL_Renderer* RENDERER;

// Window Object and Mutex
extern WINDOW _W;
extern mutex _WMutex;

// Renderer Object and Mutex
extern RENDERER _R;
extern mutex _RMutex;

// Keyboard Input mutex
extern mutex _KeyboardMutex;

// Framerate manager and mutex
extern FPSmanager *manager;
extern mutex _FPSMutex;

// Thread Kill Switch
extern bool KILL_THREADS;

// Window Parameters
#define WINDOW_NAME "Window"
#define WIDTH 640
#define HEIGHT 480
#define FRAMERATE 10

void error_report(const string func);

extern vector<IGraphicsObject*> gfxobjs;

class gKeyboard
{
private:
	static gKeyboard *instance;


	bool mUp;
	bool mDown; 
	bool mRight;
	bool mLeft; 
	bool mSpace;

	gKeyboard() {}
public:
	static gKeyboard* GetInstance()
	{
		if (instance == 0)
		{
			instance = new gKeyboard;
		}

		return instance;
	}

	bool UpPressed() 	
	{		
		if (mUp)
		{			
			_KeyboardMutex.lock();
			mUp = false;
			_KeyboardMutex.unlock();

			return true;
		}

		return false;
	}
	bool DownPressed() 	
	{ 
		if (mDown)
		{
			_KeyboardMutex.lock();
			mDown = false;
			_KeyboardMutex.unlock();

			return true;
		}

		return false;
	}
	bool LeftPressed() 	
	{ 
		if (mLeft)
		{
			_KeyboardMutex.lock();
			mLeft = false;
			_KeyboardMutex.unlock();

			return true;
		}

		return false;
	}
	bool RightPressed() 
	{ 
		if (mRight)
		{
			_KeyboardMutex.lock();
			mRight = false;
			_KeyboardMutex.unlock();

			return true;
		}

		return false;
	}
	bool SpacePressed() 
	{ 
		if (mSpace)
		{
			_KeyboardMutex.lock();
			mSpace = false;
			_KeyboardMutex.unlock();

			return true;
		}

		return false;
	}

	void SetUp() 
	{ 
		mUp = true; 
	}
	void SetDown() 
	{ 
		mDown = true; 
	}
	void SetLeft() 
	{ 
		mLeft = true; 
	}
	void SetRight() 
	{ 
		mRight = true; 
	}
	void SetSpace() 
	{ 
		mSpace = true; 
	}
};

#endif /* COMMON_H_ */