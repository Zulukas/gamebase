#ifndef TTEXTOBJECT_H_
#define TTEXTOBJECT_H_

// #include "sIGraphicsObject.h"
#include "common.h"

#define DEFAULT_FONT_SIZE 16
#define DEFAULT_FONT_LOCATION "/usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-R.ttf"
#define DEFAULT_TEXT "Set Me"

const SDL_Color DEFAULT_COLOR = {0, 0, 0, 0};

class TTextObject : public IGraphicsObject
{
protected:
	string mText;
	static string mFontLocation;
	int mFontSize;
	SDL_Color mColor;

	TTF_Font* mFont;

	SDL_Texture* mTexture;
	SDL_Rect mRect;

	void create_text_texture();
	void update_font();
	void validate_font();

	SDL_Texture* surface_to_texture(SDL_Surface* surface);

public:
	TTextObject(const string& Text, int FontSize, const TPoint& Origin, const SDL_Color Color);
	~TTextObject();

	void SetFont(const string& FontLocation);
	void SetFontSize(const int FontSize);
	void SetText(const string& Text);
	void SetColor(const SDL_Color& Color);

	void SetOrigin(const TPoint& origin);

	string GetText() const { return mText; }
	int GetFontSize() const { return mFontSize; }

	void draw();
	void update() {}
};

#endif /* TTEXTOBJECT_H_ */