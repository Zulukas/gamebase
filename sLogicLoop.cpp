#include "sLogicLoop.h"

#include <unistd.h>
#include "sTTextObject.h"

// #define LOGIC_LOOP_DEBUG

void LogicLoop()
{
	gKeyboard* keyboard = gKeyboard::GetInstance();

	while(!KILL_THREADS)
	{
		// cout << "...\n";
		if (keyboard->UpPressed())
		{
			#ifdef LOGIC_LOOP_DEBUG
			cout << "Up is being held.\n";
			#endif
		}
		if (keyboard->DownPressed())
		{
			#ifdef LOGIC_LOOP_DEBUG
			cout << "Down is being held.\n";
			#endif
		}
		if (keyboard->LeftPressed())
		{
			#ifdef LOGIC_LOOP_DEBUG
			cout << "Left is being held.\n";
			#endif
		}
		if (keyboard->RightPressed())
		{
			#ifdef LOGIC_LOOP_DEBUG
			cout << "Right is being held.\n";
			#endif
		}

		// unsigned char r = 50;

		// SDL_Color c = {r, 0, 0, 0};

		// TTextObject* t = dynamic_cast<TTextObject*>(gfxobjs[0]);
		// t->SetColor(c);
		// gfxobjs

		// TTextObject* t = (TTextObject)gfxobjs[0];
		// t->SetColor(c);

		// ((TTextObject)gfxobjs[0])->SetColor(c);		

		// r += 10;

		SDL_framerateDelay(manager);
	}
}
