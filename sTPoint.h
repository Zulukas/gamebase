#ifndef TPOINT_H_
#define TPOINT_H_

#include <iostream>

#include "sTVelocity.h"

class TPoint
{
public:
	int x, y;

	TPoint() { x = 0; y = 0; }
	TPoint(const int x, const int y) { this->x = x; this->y = y; }
	TPoint(const TPoint& p) { this->x = p.x; this->y = p.y; }

	TPoint operator + (const TPoint& p)
	{
		return TPoint(x + p.x, y + p.y);
	}

	TPoint operator + (const TVelocity& v)
	{
		return TPoint(x + v.dX, y + v.dY);
	}

	TPoint operator - (const TPoint& p)
	{
		return TPoint(x - p.x, y -p.y);
	}

	TPoint operator - (const TVelocity& v)
	{
		return TPoint(x - v.dX, y -v.dY);
	}

	void operator += (const TPoint& p)
	{
		this->x += p.x;
		this->y += p.y;
	}

	void operator += (const TVelocity& v)
	{
		this->x += v.dX;
		this->y += v.dY;
	}

	void operator -= (const TPoint& p)
	{
		this->x -= p.x;
		this->y -= p.y;
	}

	void operator -= (const TVelocity& v)
	{
		this->x -= v.dX;
		this->y -= v.dY;
	}
};

inline std::ostream& operator << (std::ostream& out, const TPoint& p)
{
	out << "(" << p.x << ", " << p.y << ")";

	return out;
}

#endif /* TPOINT_H_ */