#include "sInputHandling.h"

void EventHandler()
{
	SDL_Event event;
	gKeyboard *keyboard = gKeyboard::GetInstance();
	cout << keyboard << endl;

	while (!KILL_THREADS)
	{
		SDL_WaitEvent(&event);		

		const Uint8 *state = SDL_GetKeyboardState(NULL);

		if (event.type == SDL_KEYUP)
		{
			if (event.key.keysym.sym == SDLK_SPACE)
			{
				// cout << "Space!\n";
			}
		}
		if (event.type == SDL_KEYDOWN)
		{
			_KeyboardMutex.lock();

			if (state[SDL_SCANCODE_UP])
			{
				// cout << "\t\tUp key pressed\n";
				keyboard->SetUp();
			}
			if (state[SDL_SCANCODE_DOWN])
			{
				// cout << "\t\tDown key pressed\n";
				keyboard->SetDown();
			}
			if (state[SDL_SCANCODE_LEFT])
			{
				// cout << "\t\tLeft key pressed\n";
				keyboard->SetLeft();
			}
			if (state[SDL_SCANCODE_RIGHT])
			{
				// cout << "\t\tRight key pressed\n";
				keyboard->SetRight();
			}

			_KeyboardMutex.unlock();
		}
		if (event.type == SDL_QUIT)
		{
			KILL_THREADS = true;			
		}							
	}
}