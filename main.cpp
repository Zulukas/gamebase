#include <iostream>
#include "common.h"
#include "sIGraphicsObject.h"
#include "sTTextObject.h"
#include "sInputHandling.h"
#include "sLogicLoop.h"
#include "sGraphicsLoop.h"
#include "sTPoint.h"

// #define GFX_DELAY_START

#ifdef GFX_DELAY_START
#include <unistd.h>
#endif

using namespace std;

WINDOW _W;
RENDERER _R;
mutex _WMutex;
mutex _RMutex;
mutex _KeyboardMutex;
FPSmanager* manager;
mutex _FPSMutex;

vector<IGraphicsObject*> gfxobjs;

bool KILL_THREADS;
gKeyboard *gKeyboard::instance = 0;

void error_report(const string func)
{
	cerr << func << " Error: " << SDL_GetError() << endl;
	exit(1);
}

void Init_FPSManager()
{
	manager = new FPSmanager;
	SDL_initFramerate(manager);
	SDL_setFramerate(manager, FRAMERATE);
}

void init_window()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
	{
		error_report("SDL_Init");
	}

	_WMutex.lock();
	_W = SDL_CreateWindow(WINDOW_NAME, 
		SDL_WINDOWPOS_UNDEFINED, 
		SDL_WINDOWPOS_UNDEFINED, 
		WIDTH,
		HEIGHT,
		SDL_WINDOW_SHOWN);

	if (_W == nullptr)
	{
		_WMutex.unlock();
		error_report("SDL_CreateWindow");
	}

	_WMutex.unlock();
}

void init_renderer()
{
	_RMutex.lock();
	_WMutex.lock();

	_R = SDL_CreateRenderer(_W, 
		-1, 
		SDL_RENDERER_ACCELERATED);

	if (_R == nullptr)
	{
		_WMutex.unlock();
		_RMutex.unlock();
		error_report("SDL_CreateRenderer");
	}

	SDL_RenderSetLogicalSize(_R, WIDTH, HEIGHT);
	SDL_SetRenderDrawColor(_R, 0, 0, 0, 255);

	_WMutex.unlock();
	_RMutex.unlock();
}

void init_ttf()
{
	if (TTF_Init() == -1)
	{
		error_report("TTF_Init");
	}
}

void cleanup()
{
	_RMutex.lock();
	_WMutex.lock();
	_FPSMutex.lock();

	SDL_DestroyRenderer(_R);
	SDL_DestroyWindow(_W);
	delete manager;
	SDL_Quit();	

	_FPSMutex.unlock();
	_WMutex.unlock();
	_RMutex.unlock();
}

void UpdateGame()
{
	static gKeyboard* keyboard = gKeyboard::GetInstance();
	if (keyboard->UpPressed())
	{
	}
	if (keyboard->DownPressed())
	{
	}
	if (keyboard->LeftPressed())
	{
	}
	if (keyboard->RightPressed())
	{
	}
}

void DrawGame()
{
	// cout << "asdf\n";
	// for (int i = 0; i < gfxobjs.size() - 1; i++)
	// {
	// 	gfxobjs[i]->draw();
	// }

	SDL_SetRenderDrawColor(_R, 0, 0, 0, 0);
	SDL_RenderClear(_R);

	for (int i = 0; i < gfxobjs.size(); i++)
	{
		gfxobjs[i]->draw();
	}

	SDL_RenderPresent(_R);
}

void GameLoop()
{
	static unsigned char r = 50;
	int count = 1;

	while(!KILL_THREADS)
	{
		UpdateGame();
		DrawGame();


		SDL_Color c = {r, 0, 0, 0};

		TTextObject* t = dynamic_cast<TTextObject*>(gfxobjs[0]);
		t->SetColor(c);	

		// TPoint p = t->GetOrigin();
		// p.y += 10;

		// t->SetOrigin(p);
		t->SetText(to_string(count++));

		r += 10;

		SDL_framerateDelay(manager);
	}
}

int main()
{
	KILL_THREADS = false;
	
	init_window();
	init_renderer();
	init_ttf();

	TTextObject *text = new TTextObject("", 30, TPoint(0,0), {255,0,0});
	
	gfxobjs.push_back(text);

	#ifdef GFX_DELAY_START

	cout << "Renderer: " << _R << endl;
	usleep(1000000);

	#endif

	thread event_thread(EventHandler);

	Init_FPSManager();

	// thread logic_thread(LogicLoop);	
	
	GameLoop();

	event_thread.join();
	// logic_thread.join();	

	//Cleanup any remaining graphics objects
	for (int i = 0; i < gfxobjs.size(); i++)
	{
		delete gfxobjs[i];
	}

	cleanup();

	return 0;
}