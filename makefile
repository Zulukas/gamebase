FLAGS=-lSDL2_gfx -lSDL2_ttf -std=c++14 `sdl2-config --libs --cflags` -g -pthread
HEADERS=sIMoveableObject.h sTPoint.h sTVelocity.h sTTextObject.h common.h sIGraphicsObject.h sLogicLoop.h sInputHandling.h sGraphicsLoop.h

OBJECTS=ttextobject.o sinputhandling.o logicloop.o graphicsloop.o main.o

a.out: $(OBJECTS)
	g++ $(OBJECTS) $(FLAGS)

graphicsloop.o: sGraphicsLoop.cpp $(HEADERS)
	g++ -c sGraphicsLoop.cpp $(FLAGS) -o graphicsloop.o

logicloop.o: sLogicLoop.cpp $(HEADERS)
	g++ -c sLogicLoop.cpp $(FLAGS) -o logicloop.o

ttextobject.o: sTTextObject.cpp $(HEADERS)
	g++ -c sTTextObject.cpp $(FLAGS) -o ttextobject.o

sinputhandling.o: sInputHandling.cpp $(HEADERS)
	g++ -c sInputHandling.cpp $(FLAGS) -o sinputhandling.o

main.o: main.cpp $(HEADERS)
	g++ -c main.cpp $(FLAGS) -o main.o


clean:
	- rm *.o a.out