#ifndef IGRAPHICSOBJECT_H_
#define IGRAPHICSOBJECT_H_

#include "sTPoint.h"
#include "common.h"

class IGraphicsObject
{
protected:
	TPoint mOrigin;
public:
	IGraphicsObject() : mOrigin(0,0) {}
	IGraphicsObject(const TPoint& origin) : mOrigin(origin) {}
	~IGraphicsObject() {}
	
	virtual void SetOrigin(const TPoint& origin) { this->mOrigin = origin; }
	virtual TPoint GetOrigin() { return mOrigin; }

	virtual void draw() = 0;
	virtual void update() = 0;
};

#endif /* IGRAPHICSOBJECT_H_ */