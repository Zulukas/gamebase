#include "sGraphicsLoop.h"
#include "sTTextObject.h"

void GraphicsLoop()
{	
	while(!KILL_THREADS)
	{
		SDL_SetRenderDrawColor(_R, 0, 0, 0, 0);
		SDL_RenderClear(_R);

		for (int i = 0; i < gfxobjs.size(); i++)
		{
			gfxobjs[i]->draw();
		}
	
		SDL_RenderPresent(_R);
		
		SDL_framerateDelay(manager);
	}
}